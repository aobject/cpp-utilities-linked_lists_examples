//
//  main.cpp
//  Basic Linked Lists Utilities
//
//  Created by Justin on 4/23/19.
//  Contact:  justin at obj dot nyc
//
//  Copyright © 2019 Justin Snider. All rights reserved.
//


#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <time.h>
using namespace std;

// Function Declariations

// Linked list Node definition
struct Node
{
    int data;
    Node *link;
    
};

// Creation of name for Node pointers
typedef Node* NodePtr;

void headInsert(NodePtr& head, int theNumber);
// Precondition: The pointer variable head points to
// the head of a linked list.
// Postcondition: A new node containing theNumber
// has been added at the head of the linked list.

void createLinkedList(NodePtr& head, const vector<int>& theValues);
// Precondition: Provide a pointer that will be set to point at the head
//               Provide a vector<int> list thate will be used to create the linked list
// Postcondition: A new linked list of nodes is created. The head is returned.

void randomValues(const int& inlistLength, const int& min, const int& max, vector<int>& outList);
// Precondition: provide a list length, a min value, max value, and a vector<int> where the values will be written.
// Postcondition: outList is populated with a set of random values.

void printAllNodes(const NodePtr& head);
// This function prints all values and links of given linked list to screen;
// Precondition: Provide the head of the linked list.
// Postcondition: All nodes of the linked list are printed to the screen.

bool searchInt(const NodePtr& head, const int& searchVal);
// Precondition: Provide the head of the linked list to search anbd a value to search for.
// Postcondition: The function returns true if the value is found otherwise false.

void addValue(const int& inValue, const int& index, NodePtr& head);
// Precondition: Provide the value to add, the index where you would like the value added, and the head of the list where the node will be added.
// Postcondition: The new node is added to the list.

void removeNode(const int& inValue, NodePtr& head);
// Precondition: Provide index of the node to remove and the head of the list where the value will be removed from.
// Postcondition: The node is removed from the list and deleted.

// Requires libraries: <iostream>
int main()
{
    // Start test of linked list utility functions
    vector<int> testList;
    NodePtr head;
    randomValues(10, -10, 10, testList);
    createLinkedList(head, testList);
    printAllNodes(head);
    if(searchInt(head, 5))
    {
        cout << "Value found." << endl;
    } else {
        cout << "Value not found." << endl;
    }
    
    addValue(5, 10, head);
    
    if(searchInt(head, 5))
    {
        cout << "Value found." << endl;
    } else {
        cout << "Value not found." << endl;
    }
    printAllNodes(head);
    
    removeNode(-10, head);
    
    if(searchInt(head, 5))
    {
        cout << "Value found." << endl;
    } else {
        cout << "Value not found." << endl;
    }
    
    printAllNodes(head);
    
    // Test of linked list utility functions completed
    
    return 0;
}

// Function Definitions

void headInsert(NodePtr& head, int theNumber)
{
    NodePtr tempPtr; // declare pointer
    tempPtr = new Node; // create the new first node and assign location to ptr
    
    tempPtr->data = theNumber; // assign value given number to new first Node
    
    tempPtr->link = head; // set node pointer to point at the new second node (the previous first node)
    head = tempPtr; // Set the head to point at the new first node

}

// Requires libraries: <iostream>
void createLinkedList(NodePtr& head, const vector<int>& theValues)
{
    // If value list is empty header is nullptr
    if( theValues.empty() )
    {
        head->link = nullptr;
        cout << "List is empty." << endl;
    } else { // else create next nodes
        // Create head first
        NodePtr tempPtr, tempPrevPtr; // declare pointer
        tempPtr = new Node; // create the new first node and assign location to ptr
        head = tempPtr; // set new node as head ptr
        tempPtr->link = nullptr;
        tempPtr->data = theValues.at(0);
        cout << "Data 0: " << tempPtr->data << endl;

        // Loop through all values after first
        for(int i = 1; i < theValues.size(); i++)
        {
            tempPrevPtr = tempPtr; // remember previous node
            tempPtr = nullptr; // set new node pointer to nullptr
            tempPtr = new Node; // create new node
            tempPtr->data = theValues.at(i); // assign value to new node
            cout << "Data " << i << ": " << tempPtr->data << endl;
            tempPtr->link = nullptr; // set new node link to nullptr
            tempPrevPtr->link = tempPtr; // assign pointer from last node to new node
        }
    }
}

// Requires libraries: <vector>, <stdlib.h>, and <time.h>
void randomValues(const int& inlistLength, const int& min, const int& max, vector<int>& outList){
    
    srand (static_cast<unsigned>(time(NULL))); // use time to get new random values everytime
    //srand (static_cast<unsigned>(0)); // hard code seed to get same random values everytime
    
    for(int i = 0; i < inlistLength ; i++)
    {
        outList.push_back((rand() % (max+1-min)) + min);
    }
}

// Requires libraries: <iostream>
void printAllNodes(const NodePtr& head)
{
    NodePtr currentPtr;
    int counter = 0;
    bool allPrinted = false;
    
    currentPtr = head;
    while (allPrinted == false) {
        cout << "Node " << counter << " Data: " << currentPtr->data << " and link: " << currentPtr-> link << endl;
        if(currentPtr->link != nullptr)
        {
            counter++;
            currentPtr = currentPtr->link ;
        } else {
            allPrinted = true;
        }

    }
}

// Requires libraries: <iostream>
bool searchInt(const NodePtr& head, const int& searchVal)
{
    NodePtr currentPtr;
    int counter = 0;
    bool allTested = false;
    bool valueFound = false;
    
    currentPtr = head;
    while (allTested == false) {
        if(currentPtr->data == searchVal)
        {
            valueFound = true;
            cout << "Value found at Node " << counter << endl;
        }
        if(currentPtr->link != nullptr)
        {
            counter++;
            currentPtr = currentPtr->link;
        } else {
            allTested = true;
        }
    }
    return valueFound;
}

void addValue(const int& inValue, const int& index, NodePtr& head)
{
    NodePtr tempPtr;
    tempPtr = new Node;
    tempPtr->data = inValue;
    // if index 0 set at head
    if(index == 0)
    {
        // set at head of list
        tempPtr->link = head;
        head = tempPtr;
    } else { // else search for given index
        NodePtr curPtr, prevPtr;
        prevPtr = head;
        curPtr = head->link;
        for(int i = 1; i<index ; i++)
        {
            prevPtr = curPtr;
            if(prevPtr->link != nullptr) // check if last index
            {
                curPtr = prevPtr->link;
            } else {
                curPtr = nullptr;
            }
        }
        prevPtr->link = tempPtr; // set previous pointer to point a new node
        tempPtr->link = curPtr; // set new node to point at next node or nullptr
    }
}

// Requires libraries: <iostream>
void removeNode(const int& inValue, NodePtr& head)
{
    bool lastNode = false;
    int counter = 0;
    NodePtr prevNode, nextNode;
    if(head == nullptr)
    {
        cout << "List is empty." << endl;
    } else {
        
        prevNode = head;
        nextNode = head->link;
        
        if(head->data == inValue)
        {
            delete head;
            head = nextNode;
        }
        
        while (lastNode == false) {
            
            // check for match
            if(nextNode->data == inValue)
            {
                // delete node
                prevNode->link = nextNode->link;
                delete nextNode;
                cout << "Node " <<  ++counter << " deleted." << endl;
            }
            // increment search
            if(nextNode->link == nullptr)
            {
                // at end of list
                lastNode = true;
            } else {
                prevNode = nextNode;
                nextNode = prevNode->link;
                counter++;
            }
        }
    }
}
